<%@page import="vo.MemberVO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script language="javascript" src="js/member.js"></script>
</head>
<body>

	<form action="service/member?menu=update" method="post" name="frm">
		<table>
			<tr>
				<td>아이디<input type="hidden" name="id" value="${loginUser.id}">${loginUser.id}
				</td>
			</tr>

			<tr>
				<td>비밀번호 <input type="hidden" name="originpass"
					value="${loginUser.pass}"> <input type="password"
					name="pass"></td>
			</tr>

			<tr>
				<td>비밀번호확인<input type="password" name="passCheck"></td>
			</tr>

			<tr>
				<td>이름${loginUser.name}</td>
			</tr>

			<tr>
				<td>주민번호${loginUser.pnum}</td>
			</tr>

			<tr>
				<%
					MemberVO mvo = (MemberVO) session.getAttribute("loginUser");
					String[] earr = mvo.getEmail().split("@");
					String[] parr = mvo.getPhone().split("-");
				%>
				<td>이메일<input type="text" name="femail" value="<%=earr[0]%>">@
					<input type="text" name="bemail" value="<%=earr[1]%>"> <select
					name="selectmail" onchange="selectMail()">
						<option>직접입력
						<option>naver.com
						<option>hanmail.net
						<option>gmail.com
				</select></td>
			</tr>

			<tr>
				<td>핸드폰번호<input type="text" name="phone1" value="<%=parr[0]%>">-<input
					type="text" name="phone2" value="<%=parr[1]%>">-<input
					type="text" name="phone3" value="<%=parr[2]%>"></td>
			</tr>

			<tr>
				<td>관심지역 <select id="mySpot" name="mySpot">
						<c:forEach items="${spotList}" var="list">

							<c:choose>
								<c:when test="${loginUser.myspot==list}">
									<option selected>${list}
								</c:when>
								<c:otherwise>
									<option>${list}
								</c:otherwise>
							</c:choose>
						</c:forEach>
				</select>
			</tr>

			<tr>
				<td>관심공원 <select id="myParkGu" name="myParkGu">
						<c:forEach items="${spotList}" var="list">
								<option>${list}
						</c:forEach>
				</select> <select id="myPark" name="myPark">

				</select>
				</td>
			</tr>

			<tr>

				<td><input type="submit" value="수정하기"
					onclick="return checkUpdateForm()">
					<BUTTON formaction="service/member?menu=delete"
						onclick="return confirmDeleteMember()">탈퇴하기</BUTTON> <input
					type="button" value="취소" onclick="location.href='index.jsp'"></td>
			</tr>
		</table>

	</form>

</body>
</html>