<%@ page language="java" contentType="text/html; charset=UTF-8"
      pageEncoding="UTF-8"%>  
      
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>회원 관리</title>
<script type="text/javascript">
function idOk(){
	//idCheck.jsp는 join.jsp에서 중복체크버튼을 눌러서 열어준 것이다.
	//idcheck.jsp는 join.jsp를 opener라고 부른다.
	
	opener.frm.id.value = "${data.get('id')}";
	//중복체크 하고 온 것인지를 확인하기 위한 hidden 태크에도 값을 채워 넣어야 함
	opener.frm.idCheck.value="${data.get('id')}";
	self.close();
}
</script>
</head>
<body>
    <h2> 아이디 중복확인 </h2>
    <form action="member?menu=idcheck" name="frm" method="post">
	    아이디  <input type=text name="id"> 
	  <input type="submit" value="중복 체크">
	  <br>
 	  ${data.get('result')}
 	  <c:if test="${empty data.get('vo')}">
 	 	 <input type="button" value="사용" onclick="idOk()">
 	  </c:if>
	</form>	
</body>
</html>