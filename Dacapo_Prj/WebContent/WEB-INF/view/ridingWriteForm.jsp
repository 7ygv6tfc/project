<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../header.jsp" %>
<link rel="stylesheet" href="/Dacapo_Prj/CSS/board.jsp">

<div id="writeForm">
	<form action=service/board?menu=write method="post">
		<table>
			<tr>
				<td>작성자</td>
				<td><input type="hidden" name="id" value="${loginUser.id}">${loginUser.id}</td>
			</tr>
			
			<tr>
				<td>장소</td>
				<td><select name="ridingRoad">
					<c:forEach items="${roadList}" var="list">
						<option>${list}
					</c:forEach>
				</select></td>
			</tr>
			
			<tr>
				<td>시간</td>
				<td><input type="text" name="year">년 <input type="text" name="month">월 <input type="text" name="day">일 
				<select name="time">
					<c:forEach var="i" begin="0" end="23">
						<option>${i}:00
					</c:forEach>
				</select></td>

			<tr>
				<td>제목</td>
				<td><input type="text" name="title"></td>
			</tr>

			<tr>
				<td>내용</td>
				<td><textarea rows="10" cols="20" name="content"></textarea></td>
			</tr>

			<tr>
				<td colspan=2><input type="submit" value="글쓰기"><input
					type="button" value="취소"
					onclick="location.href='service/board?menu=getAll'">
		</table>
	</form>
</div>
</body>
</html>