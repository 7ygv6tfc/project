<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="../../header.jsp"%>
<link rel="stylesheet" href="/Dacapo_Prj/CSS/board.css">
<link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/media/images/favicon.ico"/>
<link rel="/Dacapo_Prj/media/css/demo_table_jui.css">
<style type="text/css" title="currentStyle">
	@import "/Dacapo_Prj/jquery.dataTables.css";
</style>
<script src="/Dacapo_Prj/jquery.js" type="text/javascript"></script>
<script src="/Dacapo_Prj/jquery.dataTables.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
	$('#ridingList').dataTable();
});
</script>
<div id="boardList">
<table id="ridingList">
	<thead>
		<tr>
			<th class="num">번호</th>
			<th class="place">장소</th>
			<th class="time">시간</th>
			<th class="title">제목</th>
			<th class="writer">작성자</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${ridingList}" var="list">
			<tr>
				<td>${list.num}</td>
				<td>${list.place}</td>
				<td>${list.time}</td>
				<td><a href="board?menu=getAllRiding&id=${list.num}">${list.title}</a></td>
				<td>${list.id}</td>
			</tr>
		</c:forEach>
	</tbody>
	</table>
</div>

<c:if test="${loginUser.id !=null }">
	<div id="boardSelecter">

		<input id="write_Btn" type="button" value="글쓰기"
			onclick="location.href='/Dacapo_Prj/service/board?menu=readyWriteRidingForm'">

	</div>
</c:if>

</body>
</html>