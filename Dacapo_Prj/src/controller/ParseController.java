package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import service.ParseService;

public class ParseController extends AbstractController{

	private ParseService service;
	
	public void setService(ParseService service) {
		this.service = service;
	}



	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String menu = request.getParameter("menu").trim();
		
		switch(menu){
		
		case "parseAir":
			service.RemoveAirInfo();
			service.parseAirXml();
			
			return new ModelAndView("redirect:../index.jsp");
			
		case "parseFestival":
			service.parseFestivalXml();
			
			return new ModelAndView("redirect:../index.jsp");
		
		case "removeFestivalInfo":
			service.RemoveFestivalInfo();
			return new ModelAndView("redirect:../index.jsp");
		}
		
		
	
		return null;
	}

}
