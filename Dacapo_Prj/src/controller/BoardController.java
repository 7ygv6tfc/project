package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import service.BoardService;
import service.FestivalService;
import service.RidingService;
import vo.BoardVO;
import vo.ReplyVO;


public class BoardController extends AbstractController {

	private BoardService service;
	private FestivalService fservice;
	private RidingService rservice;

	public void setService(BoardService service) {
		this.service = service;
	}

	public void setFservice(FestivalService fservice) {
		this.fservice = fservice;
	}

	public void setRservice(RidingService rservice) {
		this.rservice = rservice;
	}

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		String menu = request.getParameter("menu");
		BoardVO bvo = new BoardVO();
		ReplyVO rvo = new ReplyVO();
		
		int bnum;
		
		switch (menu) {
		
/*______________________________________Q&A게시판_____________________________________*/
		
		
		//게시글 한건(+해당 글의 댓글까지)
				case "get":
					bnum = Integer.parseInt(request.getParameter("bnum"));
					request.setAttribute("qna",service.getArticle(bnum));
					request.setAttribute("reply", service.getAllReply(bnum));
					
					return new ModelAndView("qna");
		
		//전체게시글
		case "getAll":
			return new ModelAndView("qnaList","qnaList",service.getAllArticle());
			
		
		
		//게시글쓰기
		case "write":
			bvo.setId(request.getParameter("id"));
			bvo.setPass(request.getParameter("pass"));
			bvo.setTitle(request.getParameter("title"));
			bvo.setContent(request.getParameter("content"));
		
			service.insertArticle(bvo);
						
			return new ModelAndView("redirect:board?menu=getAll");
		
		//게시글 삭제
		case "delete":
			//해당게시글의 전체댓글 삭제
			service.deleteAllReply(Integer.parseInt(request.getParameter("bnum")));
			//해당게시글 삭제
			service.deleteArticle(Integer.parseInt(request.getParameter("bnum")));
			
			return new ModelAndView("redirect:board?menu=getAll");
		
		//댓글쓰기
		case "writeReply":
			rvo.setBnum(Integer.parseInt(request.getParameter("bnum")));
			rvo.setId(request.getParameter("id").trim());
			rvo.setContent(request.getParameter("content"));
			
			service.insertReply(rvo);
			
			return new ModelAndView("redirect:../service/board?menu=get&bnum="+request.getParameter("bnum"));
			
		//댓글삭제
		case "deleteReply":
			service.deleteReply(Integer.parseInt(request.getParameter("rnum")));
			
			return new ModelAndView("redirect:../service/board?menu=get&bnum="+request.getParameter("bnum"));
		
/*______________________________________축제게시판_____________________________________*/		
		//축제정보리스트
		case "getAllFestival":
			return new ModelAndView("festivalList","fList",fservice.getAll());
			
			
/*______________________________________라이딩게시판_____________________________________*/
		
		case "readyWriteRidingForm":
			return new ModelAndView("ridingWriteForm");
			
		case "getAllRiding":
			return new ModelAndView("ridingList","ridingList",rservice.RidingBoardGetAll());
			
		}	
	
	return null;
	}

}
