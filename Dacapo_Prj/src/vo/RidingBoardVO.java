package vo;

public class RidingBoardVO 
{
	private int num;
	private String title;
	private String time;
	private String place;
	private String content;
	private String memberID;
	
	public int getNum()
	{
		return num;
	}
	public void setNum(int num) 
	{
		this.num = num;
	}
	public String getTitle() 
	{
		return title;
	}
	public void setTitle(String title) 
	{
		this.title = title;
	}
	public String getTime() 
	{
		return time;
	}
	public void setTime(String time)
	{
		this.time = time;
	}
	public String getPlace() 
	{
		return place;
	}
	public void setPlace(String place) 
	{
		this.place = place;
	}
	public String getContent()
	{
		return content;
	}
	public void setContent(String content)
	{
		this.content = content;
	}
	public String getMemberID() 
	{
		return memberID;
	}
	public void setMemberID(String memberID) 
	{
		this.memberID = memberID;
	}
}
