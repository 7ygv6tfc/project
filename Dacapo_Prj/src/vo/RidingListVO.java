package vo;

public class RidingListVO 
{
	private int num;
	private int ridingBoardNum;
	private String memberID;
	private String phone;
	
	public int getNum() 
	{
		return num;
	}
	public void setNum(int num)
	{
		this.num = num;
	}
	public int getRidingBoardNum() 
	{
		return ridingBoardNum;
	}
	public void setRidingBoardNum(int ridingBoardNum) 
	{
		this.ridingBoardNum = ridingBoardNum;
	}
	public String getMemberID() 
	{
		return memberID;
	}
	public void setMemberID(String memberID) 
	{
		this.memberID = memberID;
	}
	public String getPhone()
	{
		return phone;
	}
	public void setPhone(String phone) 
	{
		this.phone = phone;
	}
}
