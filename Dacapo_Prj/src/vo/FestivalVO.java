package vo;

public class FestivalVO 
{
	private String fCode;
	private String title;
	private String place;
	private String sData;
	private String eData;
	private String content;
	private String managerID;
	
	public String getfCode() 
	{
		return fCode;
	}
	public void setfCode(String fCode) 
	{
		this.fCode = fCode;
	}
	public String getTitle() 
	{
		return title;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}
	public String getPlace() 
	{
		return place;
	}
	public void setPlace(String place) 
	{
		this.place = place;
	}
	public String getsData() 
	{
		return sData;
	}
	public void setsData(String sData) 
	{
		this.sData = sData;
	}
	public String geteData()
	{
		return eData;
	}
	public void seteData(String eData) 
	{
		this.eData = eData;
	}
	public String getContent() 
	{
		return content;
	}
	public void setContent(String content)
	{
		this.content = content;
	}
	public String getManagerID() 
	{
		return managerID;
	}
	public void setManagerID(String managerID) 
	{
		this.managerID = managerID;
	}
}
