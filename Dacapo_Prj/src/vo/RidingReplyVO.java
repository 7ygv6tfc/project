package vo;

public class RidingReplyVO
{
	private int num;
	private String content;
	private String memberID;
	private int ridingBoardNum;
	
	public int getNum() 
	{
		return num;
	}
	public void setNum(int num) 
	{
		this.num = num;
	}
	public String getContent()
	{
		return content;
	}
	public void setContent(String content) 
	{
		this.content = content;
	}
	public String getMemberID() 
	{
		return memberID;
	}
	public void setMemberID(String memberID) 
	{
		this.memberID = memberID;
	}
	public int getRidingBoardNum() 
	{
		return ridingBoardNum;
	}
	public void setRidingBoardNum(int ridingBoardNum) 
	{
		this.ridingBoardNum = ridingBoardNum;
	}
}
