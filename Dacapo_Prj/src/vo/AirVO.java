package vo;

public class AirVO 
{
	private String regionName;
	private int airIndex;
	private String indexGrade;
	private float nitrogen;
	private float ozone;
	private float sulfurous;
	private int pm10;
	private int pm25;
	
	public String getRegionName()
	{
		return regionName;
	}
	public void setRegionName(String regionName)
	{
		this.regionName = regionName;
	}
	public int getAirIndex() 
	{
		return airIndex;
	}
	public void setAirIndex(int airIndex)
	{
		this.airIndex = airIndex;
	}
	public String getIndexGrade() 
	{
		return indexGrade;
	}
	public void setIndexGrade(String indexGrade) 
	{
		this.indexGrade = indexGrade;
	}
	public float getNitrogen() 
	{
		return nitrogen;
	}
	public void setNitrogen(float nitrogen) 
	{
		this.nitrogen = nitrogen;
	}
	public float getOzone()
	{
		return ozone;
	}
	public void setOzone(float ozone) 
	{
		this.ozone = ozone;
	}
	public float getSulfurous() 
	{
		return sulfurous;
	}
	public void setSulfurous(float sulfurous)
	{
		this.sulfurous = sulfurous;
	}
	public int getPm10()
	{
		return pm10;
	}
	public void setPm10(int pm10)
	{
		this.pm10 = pm10;
	}
	public int getPm25() 
	{
		return pm25;
	}
	public void setPm25(int pm25) 
	{
		this.pm25 = pm25;
	}
}
