package dao;

import java.util.List;

import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import vo.BoardVO;
import vo.ReplyVO;


import com.ibatis.sqlmap.client.SqlMapClient;

public class BoardDAOImpl extends SqlMapClientDaoSupport implements IBoardDAO{

	@Override
	public void insert(BoardVO bvo) {
		getSqlMapClientTemplate().insert("insertQA",bvo);
		
	}

	@Override
	public void update(BoardVO bvo) {
		getSqlMapClientTemplate().update("updateQA",bvo);
		
	}

	@Override
	public void delete(int num) {
		getSqlMapClientTemplate().delete("deleteQA",num);
		
	}

	@Override
	public BoardVO get(int num) {
		// TODO Auto-generated method stub
		return (BoardVO)getSqlMapClientTemplate().queryForObject("getQA", num);
	}

	@Override
	public List<BoardVO> getAll() {
		// TODO Auto-generated method stub
		return (List<BoardVO>)getSqlMapClientTemplate().queryForList("getAllQA");
	}

	@Override
	public ReplyVO getReply(int rnum) {
		// TODO Auto-generated method stub
		return (ReplyVO)getSqlMapClientTemplate().queryForObject("getQA", rnum);
	}

	@Override
	public List<ReplyVO> getAllReply(int bnum) {
		// TODO Auto-generated method stub
		return (List<ReplyVO>)getSqlMapClientTemplate().queryForList("getAllQR",bnum);
	}

	@Override
	public void insertReply(ReplyVO rvo) {
		getSqlMapClientTemplate().insert("insertQR",rvo);
		
	}

	@Override
	public void updateReply(ReplyVO rvo) {
		getSqlMapClientTemplate().update("updateQR",rvo);
		
	}

	@Override
	public void deleteReply(int rnum) {
		getSqlMapClientTemplate().delete("deleteQR",rnum);
		
	}

	@Override
	public void deleteAllReply(int bnum) {
		getSqlMapClientTemplate().delete("deleteAllQR",bnum);
		
	}

}
