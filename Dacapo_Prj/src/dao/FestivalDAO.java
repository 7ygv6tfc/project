package dao;

import java.util.List;

import vo.FestivalVO;

public class FestivalDAO 
{
	private IFestivalDAO fdao;
	

	public void setFdao(IFestivalDAO fdao) {
		this.fdao = fdao;
	}


	public List<FestivalVO> getAll()
	{
		return fdao.getAll();
	}
}
