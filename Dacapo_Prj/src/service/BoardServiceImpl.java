package service;

import java.util.List;

import vo.BoardVO;
import vo.ReplyVO;
import dao.BoardDAO;



public class BoardServiceImpl implements IBoardService {

	private BoardDAO bdao;
	
	public void setBdao(BoardDAO bdao) {
		this.bdao = bdao;
	}

	@Override
	public void insert(BoardVO bvo) {
		bdao.insertArticle(bvo);
		
	}

	@Override
	public void update(BoardVO bvo) {
		bdao.updateArticle(bvo);
		
	}

	@Override
	public void delete(int num) {
		bdao.deleteArticle(num);
		
	}

	@Override
	public BoardVO get(int num) {
		// TODO Auto-generated method stub
		return bdao.getArticle(num);
	}

	@Override
	public List<BoardVO> getAll() {
		// TODO Auto-generated method stub
		return bdao.getAllArticles();
	}

	@Override
	public void insertReply(ReplyVO rvo) {
		bdao.insertReply(rvo);
		
	}

	@Override
	public void updateReply(ReplyVO rvo) {
		bdao.updateReply(rvo);
		
	}

	@Override
	public void deleteReply(int rnum) {
		bdao.deleteReply(rnum);
		
	}

	@Override
	public ReplyVO getReply(int rnum) {
		// TODO Auto-generated method stub
		return bdao.getReply(rnum);
	}

	@Override
	public List<ReplyVO> getAllReply(int bnum) {
		// TODO Auto-generated method stub
		return bdao.getAllReply(bnum);
	}

	@Override
	public void deleteAllReply(int bnum) {
		// TODO Auto-generated method stub
		bdao.deleteAllReply(bnum);
	}
	

}
