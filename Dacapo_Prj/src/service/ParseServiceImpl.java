package service;

import java.io.InputStream;
import java.net.URL;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import vo.AirVO;
import vo.BikeRoadVO;
import vo.FestivalVO;
import dao.ParseDAO;


public class ParseServiceImpl implements IParseService
{
	private ParseDAO pdao;
	

	public void setPdao(ParseDAO pdao) {
		this.pdao = pdao;
	}

	public void parseAirXml()
	{
		AirVO aVo=new AirVO();
		String spec="http://openapi.seoul.go.kr:8088/657966674a7073683133307948594a59/xml/ListAirQualityByDistrictService/1/25/";
		//xml URL 등록
		DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
		//Dom 개체를 저장할 수 있는 객체
		
		try 
		{
			DocumentBuilder parse=factory.newDocumentBuilder();
			//Dom 객체를 파싱할 수 있는 빌더 생성
			
			URL url=new URL(spec);
			//인터넷 url 을 나타내는 객체생성
			InputStream in=url.openStream();
			//URL 을 읽을 수 있는 입력 스트림 형성
			
			Document doc=parse.parse(in);
			//입력스트림으로 부터 읽은 Document 저장 객체 생성  
			Element element=doc.getDocumentElement();
			//XML모든 객체를 얻어 저장한다.
			
			NodeList nList=element.getElementsByTagName("list_total_count");
			Element name;
			int cnt=0;
			for(int i=0;i<nList.getLength();i++)
			{
				name=(Element)nList.item(i);
				cnt=Integer.parseInt(name.getTextContent());
			}
			for(int i=0;i<cnt;i++)
			{
				nList=element.getElementsByTagName("MSRSTENAME");
				name=(Element)nList.item(i);
				aVo.setRegionName(name.getTextContent());
				
				nList=element.getElementsByTagName("MAXINDEX");
				name=(Element)nList.item(i);
				if(!(name.getTextContent().equals("점검중") ||name.getTextContent().equals("-")))
					aVo.setAirIndex(Integer.parseInt(name.getTextContent()));
				nList=element.getElementsByTagName("GRADE");
				name=(Element)nList.item(i);
				aVo.setIndexGrade(name.getTextContent());
				
				nList=element.getElementsByTagName("NITROGEN");
				name=(Element)nList.item(i);
				if(!name.getTextContent().equals("점검중"))
					aVo.setNitrogen(Float.valueOf(name.getTextContent()));
				
				nList=element.getElementsByTagName("SULFUROUS");
				name=(Element)nList.item(i);
				if(!name.getTextContent().equals("점검중"))
					aVo.setOzone(Float.valueOf(name.getTextContent()));
				
				nList=element.getElementsByTagName("OZONE");
				name=(Element)nList.item(i);
				if(!name.getTextContent().equals("점검중"))
					aVo.setOzone(Float.valueOf(name.getTextContent()));
				
				nList=element.getElementsByTagName("PM10");
				name=(Element)nList.item(i);
				if(!name.getTextContent().equals("점검중"))
					aVo.setPm10(Integer.parseInt(name.getTextContent()));
				
				nList=element.getElementsByTagName("PM25");
				name=(Element)nList.item(i);
				if(!name.getTextContent().equals("점검중"))
					aVo.setPm25(Integer.parseInt(name.getTextContent()));
				
				pdao.parseAirXml(aVo);
			}
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
	}
	public void ParseFestivalXml()
	{
		FestivalVO fVo=new FestivalVO();
		String spec="http://openapi.seoul.go.kr:8088/544872554a707368373574727a5a63/xml/SearchCulturalActivityService/1/50/";
		//xml URL 등록
		DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
		//Dom 개체를 저장할 수 있는 객체
		
		try 
		{
			DocumentBuilder parse=factory.newDocumentBuilder();
			//Dom 객체를 파싱할 수 있는 빌더 생성
			
			URL url=new URL(spec);
			//인터넷 url 을 나타내는 객체생성
			InputStream in=url.openStream();
			//URL 을 읽을 수 있는 입력 스트림 형성
			
			Document doc=parse.parse(in);
			//입력스트림으로 부터 읽은 Document 저장 객체 생성  
			Element element=doc.getDocumentElement();
			//XML모든 객체를 얻어 저장한다.
			
			NodeList nList=element.getElementsByTagName("list_total_count");
			Element name;
			int cnt=0;
			for(int i=0;i<nList.getLength();i++)
			{
				name=(Element)nList.item(i);
				cnt=Integer.parseInt(name.getTextContent());
			}
			for(int i=0;i<cnt;i++)
			{
				nList=element.getElementsByTagName("NUM");
				name=(Element)nList.item(i);
				fVo.setfCode(name.getTextContent());
				
				nList=element.getElementsByTagName("TITLE");
				name=(Element)nList.item(i);
				fVo.setTitle(name.getTextContent());
				
				nList=element.getElementsByTagName("PLACE");
				name=(Element)nList.item(i);
				fVo.setPlace(name.getTextContent());
				
				nList=element.getElementsByTagName("SDATE");
				name=(Element)nList.item(i);
				fVo.setsData(name.getTextContent());
				
				nList=element.getElementsByTagName("EDATE");
				name=(Element)nList.item(i);
				fVo.seteData(name.getTextContent());
				
				nList=element.getElementsByTagName("REMARK");
				name=(Element)nList.item(i);
				fVo.setContent(name.getTextContent());
				
				fVo.setManagerID("dacapo");
				
				pdao.ParseFestivalXml(fVo);
			}
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
	}

	@Override
	public void RemoveAirInfo() {
		pdao.removeAirInfo();
		
	}

	@Override
	public void RemoveFestivalRoadInfo() {
		pdao.removeFestivalInfo();
		
	}

	
}
