package service;

import java.util.List;

import vo.FestivalVO;

public class FestivalService 
{
	private IFestivalService service;
	
	public void setService(IFestivalService service) 
	{
		this.service=service;
	}
	public List<FestivalVO> getAll()
	{
		return service.getAll();
	}
}
