package service;


public interface IParseService 
{
	public void parseAirXml();
	public void ParseFestivalXml();
	public void RemoveAirInfo();
	public void RemoveFestivalRoadInfo();
}
