package service;

import java.util.List;

import vo.MemberVO;

public class MemberService {
	
	private IMemberService service;
	
	public void setService(IMemberService service) {
		this.service = service;
	}

	public void insertMember(MemberVO mvo){
		service.insertMember(mvo);
	}
	
	public void deleteMember(MemberVO mvo){
		service.deleteMember(mvo);
	}
	
	public void updateMember(MemberVO mvo){
		service.updateMember(mvo);
	}

	public MemberVO getMember(String id){
		return service.getMember(id);
	}
	
	public List<MemberVO> getAllMembers(){
		return service.getAllMembers();
	}
	
	public List<String> getSpots(){
		return service.getSpots();
	}
	
	public List<String> getParks(String spot){
		return service.getParks(spot);
	}
}
